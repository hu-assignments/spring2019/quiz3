import league.*;
import util.FixtureManager;

import java.io.FileWriter;

public class Main {
    public static void main(String[] args) {

        String fixturesFilename = args[0];
        String footballOutputFilename = args[1];
        String basketballOutputFilename = args[2];
        String volleyballOutputFilename = args[3];

        try {
            FixtureManager fixtureManager = new FixtureManager(fixturesFilename);
            Fixture footballFixture = fixtureManager.getFootballFixture();
            Fixture basketballFixture = fixtureManager.getBasketballFixture();
            Fixture volleyballFixture = fixtureManager.getVolleyBallFixture();

            League footballLeague = new FootballLeague(footballFixture);
            League basketballLeague = new BasketballLeague(basketballFixture);
            League volleyballLeague = new VolleyballLeague(volleyballFixture);

            FileWriter fileWriter = new FileWriter(footballOutputFilename);
            fileWriter.write(footballLeague.toString());
            fileWriter.close();

            fileWriter = new FileWriter(basketballOutputFilename);
            fileWriter.write(basketballLeague.toString());
            fileWriter.close();

            fileWriter = new FileWriter(volleyballOutputFilename);
            fileWriter.write(volleyballLeague.toString());
            fileWriter.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}