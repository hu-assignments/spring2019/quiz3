package match;

import league.team.Team;

public abstract class Match {
    private String mHomeTeamName;
    private String mAwayTeamName;
    private int mHomeScore;

    private int mAwayScore;

    public Match(String homeTeamName, String awayTeamName, int homeScore, int awayScore) {
        mHomeTeamName = homeTeamName;
        mAwayTeamName = awayTeamName;
        mHomeScore = homeScore;
        mAwayScore = awayScore;
    }

    public abstract int getHomePoint();

    public abstract int getAwayPoint();

    public String getHomeTeamName() {
        return mHomeTeamName;
    }

    public String getAwayTeamName() {
        return mAwayTeamName;
    }

    public int getHomeScore() {
        return mHomeScore;
    }

    public int getAwayScore() {
        return mAwayScore;
    }

    public boolean isHome(Team team) {
        return team.getName().equals(getHomeTeamName());
    }
}
