package match;

public class FootballMatch extends Match {
    public FootballMatch(String homeTeamName, String awayTeamName, int homeScore, int awayScore) {
        super(homeTeamName, awayTeamName, homeScore, awayScore);
    }

    @Override
    public int getHomePoint() {
        return (getHomeScore() > getAwayScore()) ? 3 : (getHomeScore() == getAwayScore()) ? 1 : 0;
    }

    @Override
    public int getAwayPoint() {
        return (getAwayScore() > getHomeScore()) ? 3 : (getAwayScore() == getHomeScore()) ? 1 : 0;
    }
}
