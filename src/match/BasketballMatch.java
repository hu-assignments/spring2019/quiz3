package match;

public class BasketballMatch extends Match {
    public BasketballMatch(String homeTeamName, String awayTeamName, int homeScore, int awayScore) {
        super(homeTeamName, awayTeamName, homeScore, awayScore);
    }

    @Override
    public int getHomePoint() {
        return (getHomeScore() > getAwayScore()) ? 2 : 1;

    }

    @Override
    public int getAwayPoint() {
        return (getAwayScore() > getHomeScore()) ? 2 : 1;
    }
}
