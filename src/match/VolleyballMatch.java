package match;

public class VolleyballMatch extends Match {
    public VolleyballMatch(String homeTeamName, String awayTeamName, int homeScore, int awayScore) {
        super(homeTeamName, awayTeamName, homeScore, awayScore);
    }

    @Override
    public int getHomePoint() {
        return (getHomeScore() == 3 && getAwayScore() < 2) ? 3 :
                (getHomeScore() == 3 && getAwayScore() == 2) ? 2 :
                        (getHomeScore() == 2 && getAwayScore() == 3) ? 1 : 0;
    }

    @Override
    public int getAwayPoint() {
        return (getAwayScore() == 3 && getHomeScore() < 2) ? 3 :
                (getAwayScore() == 3 && getHomeScore() == 2) ? 2 :
                        (getAwayScore() == 2 && getHomeScore() == 3) ? 1 : 0;
    }
}
