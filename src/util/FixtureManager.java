package util;

import league.Fixture;
import match.BasketballMatch;
import match.FootballMatch;
import match.Match;
import match.VolleyballMatch;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FixtureManager {
    private String mFilename;
    private ArrayList<Match> mMatches;

    public FixtureManager(String filename) throws IOException {
        mFilename = filename;
        mMatches = new ArrayList<>();
        build();
    }

    private void build() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(mFilename));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            try {
                if (line.length() > 0)
                    this.addMatch(line);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        bufferedReader.close();
    }

    public Fixture getFootballFixture() {
        Fixture fixture = new Fixture();
        for (Match match : mMatches) {
            if (match instanceof FootballMatch) fixture.addMatch(match);
        }
        return fixture;
    }

    public Fixture getBasketballFixture() {
        Fixture fixture = new Fixture();
        for (Match match : mMatches) {
            if (match instanceof BasketballMatch) fixture.addMatch(match);
        }
        return fixture;
    }

    public Fixture getVolleyBallFixture() {
        Fixture fixture = new Fixture();
        for (Match match : mMatches) {
            if (match instanceof VolleyballMatch) fixture.addMatch(match);
        }
        return fixture;
    }


    private void addMatch(String matchString) {
        String[] matchStringArr = matchString.split("\t");
        String type = matchStringArr[0].trim();
        String homeTeamName = matchStringArr[1].trim();
        String awayTeamName = matchStringArr[2].trim();
        int homeScore = Integer.valueOf(matchStringArr[3].split(" : ")[0].trim());
        int awayScore = Integer.valueOf(matchStringArr[3].split(" : ")[1].trim());

        Match match = null;
        switch (type) {
            case "F":
                match = new FootballMatch(homeTeamName, awayTeamName, homeScore, awayScore);
                break;
            case "B":
                match = new BasketballMatch(homeTeamName, awayTeamName, homeScore, awayScore);
                break;
            case "V":
                match = new VolleyballMatch(homeTeamName, awayTeamName, homeScore, awayScore);
                break;
        }

        mMatches.add(match);
    }
}
