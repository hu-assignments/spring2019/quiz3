package league;

import league.team.Team;
import match.Match;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public abstract class League {

    private ArrayList<Team> mStandings;
    private Fixture mFixture;
    private LinkedHashSet<Team> mTeams;

    public League(Fixture fixture) {
        mTeams = new LinkedHashSet<>();
        mFixture = fixture;
        mStandings = new ArrayList<>();
        buildTeamList();
        buildStandings();
    }

    public Fixture getFixture() {
        return mFixture;
    }

    public void addTeam(Team team) {
        mTeams.add(team);
    }

    public abstract void buildTeamList();

    public void evaluateTeamScores() {
        for (Team team : getTeams()) {
            for (Match match : team.getMatches()) {
                boolean isHome = match.isHome(team);
                team.addPoint((isHome) ? match.getHomePoint() : match.getAwayPoint());
                team.addScoresFor((isHome) ? match.getHomeScore() : match.getAwayScore());
                team.addScoresAgainst((isHome) ? match.getAwayScore() : match.getHomeScore());
            }
        }

        getStandings().addAll(getTeams());
    }

    private void buildStandings() {
        getStandings().sort(Team::compareTo);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < getStandings().size(); i++) {
            builder.append(getStandings().get(i));
            if (i != getStandings().size() - 1) {
                builder.append("\n");
            }
        }
        return builder.toString();
    }

    public LinkedHashSet<Team> getTeams() {
        return mTeams;
    }

    public ArrayList<Team> getStandings() {
        return mStandings;
    }

}
