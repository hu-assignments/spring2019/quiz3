package league.team;

import league.League;

public class BasketballTeam extends Team {
    public BasketballTeam(String name, League league) {
        super(name, league);
    }

    @Override
    public int hashCode() {
        return getName().concat(" / Basketball Team").hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BasketballTeam)
            return ((BasketballTeam) o).getName().equals(getName());
        return false;
    }

}
