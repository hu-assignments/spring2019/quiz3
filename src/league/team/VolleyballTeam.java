package league.team;

import league.League;

public class VolleyballTeam extends Team {
    public VolleyballTeam(String name, League league) {
        super(name, league);
    }

    @Override
    public int hashCode() {
        return getName().concat(" / Volleyball Team").hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof VolleyballTeam)
            return ((VolleyballTeam) o).getName().equals(getName());
        return false;
    }
}
