package league.team;

import league.League;
import match.Match;

import java.util.ArrayList;

public abstract class Team implements Comparable<Team> {
    private String mName;
    private int mPoints;
    private int mScoreDifference;
    private int mScoresAgainst;
    private int mScoresFor;
    private ArrayList<Match> mMatches;
    private League mLeague;

    public Team(String name, League league) {
        mName = name;
        mLeague = league;
        mMatches = new ArrayList<>();
        mPoints = 0;
        buildMatchList();
    }

    public String getName() {
        return mName;
    }

    public ArrayList<Match> getMatches() {
        return mMatches;
    }

    public League getLeague() {
        return mLeague;
    }

    public void addPoint(int point) {
        mPoints += point;
    }

    public void addScoresAgainst(int against) {
        mScoresAgainst += against;
    }

    public void addScoresFor(int scoresFor) {
        mScoresFor += scoresFor;
    }

    public int getPoints() {
        return mPoints;
    }

    public int getScoreDifference() {
        return getScoresFor() - getScoresAgainst();
    }

    public int getScoresAgainst() {
        return mScoresAgainst;
    }

    public int getScoresFor() {
        return mScoresFor;
    }

    public void buildMatchList() {
        for (Match match : getLeague().getFixture().getMatches()) {
            if (match.getHomeTeamName().equals(getName()) || match.getAwayTeamName().equals(getName())) {
                getMatches().add(match);
            }
        }
    }

    public int getRank() {
        return getLeague().getStandings().indexOf(this) + 1;
    }

    public int getPlayedMatches() {
        return getMatches().size();
    }

    public int getMatchesWon() {
        int matchesWon = 0;
        for (Match match : getMatches()) {
            matchesWon += (match.isHome(this) ? match.getHomeScore() > match.getAwayScore() : match.getAwayScore() > match.getHomeScore()) ? 1 : 0;
        }
        return matchesWon;
    }


    public int getMatchesLost() {
        int matchesLost = 0;
        for (Match match : getMatches()) {
            matchesLost += (match.isHome(this) ? match.getHomeScore() < match.getAwayScore() : match.getAwayScore() < match.getHomeScore()) ? 1 : 0;
        }
        return matchesLost;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getRank()).append(".\t");
        builder.append(getName()).append("\t");
        builder.append(getPlayedMatches()).append("\t");
        builder.append(getMatchesWon()).append("\t");
        builder.append(getMatchesLost()).append("\t");
        builder.append(getScoresFor()).append(":").append(getScoresAgainst()).append("\t");
        builder.append(getPoints());
        return builder.toString();
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object o);

    @Override
    public int compareTo(Team team) {
        if (getPoints() == team.getPoints()) {
            return team.getScoreDifference() - getScoreDifference();
        }
        return team.getPoints() - getPoints();
    }

}
