package league.team;

import league.League;
import match.Match;

public class FootballTeam extends Team {


    public FootballTeam(String name, League league) {
        super(name, league);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FootballTeam)
            return ((FootballTeam) o).getName().equals(getName());
        return false;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getRank()).append(".\t");
        builder.append(getName()).append("\t");
        builder.append(getPlayedMatches()).append("\t");
        builder.append(getMatchesWon()).append("\t");
        builder.append(getMatchesTie()).append("\t");
        builder.append(getMatchesLost()).append("\t");
        builder.append(getScoresFor()).append(":").append(getScoresAgainst()).append("\t");
        builder.append(getPoints());
        return builder.toString();
    }

    public int getMatchesTie() {
        int tieMatches = 0;
        for (Match match : getMatches()) {
            tieMatches += (match.getHomeScore() == match.getAwayScore()) ? 1 : 0;
        }
        return tieMatches;
    }

    @Override
    public int hashCode() {
        return getName().concat(" / Football Team").hashCode();
    }
}
