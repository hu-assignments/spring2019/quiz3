package league;

import league.team.VolleyballTeam;
import match.Match;

public class VolleyballLeague extends League {
    public VolleyballLeague(Fixture fixture) {
        super(fixture);
    }

    @Override
    public void buildTeamList() {
        for (Match volleyballMatch : getFixture().getMatches()) {
            addTeam(new VolleyballTeam(volleyballMatch.getHomeTeamName(), this));
        }

        evaluateTeamScores();
    }
}
