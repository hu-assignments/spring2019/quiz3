package league;

import match.Match;

import java.util.ArrayList;

public class Fixture {

    private ArrayList<Match> mMatches;

    public Fixture() {
        mMatches = new ArrayList<>();
    }

    public void addMatch(Match match) {
        mMatches.add(match);
    }

    public ArrayList<Match> getMatches() {
        return mMatches;
    }
}
