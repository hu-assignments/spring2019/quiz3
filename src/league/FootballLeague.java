package league;

import league.team.FootballTeam;
import match.Match;

public class FootballLeague extends League {
    public FootballLeague(Fixture fixture) {
        super(fixture);
    }

    @Override
    public void buildTeamList() {
        for (Match footballMatch : getFixture().getMatches()) {
            addTeam(new FootballTeam(footballMatch.getHomeTeamName(), this));
        }

        evaluateTeamScores();
    }

}
