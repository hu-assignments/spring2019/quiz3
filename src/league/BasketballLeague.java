package league;

import league.team.BasketballTeam;
import match.Match;

public class BasketballLeague extends League {
    public BasketballLeague(Fixture fixture) {
        super(fixture);
    }

    @Override
    public void buildTeamList() {
        for (Match basketballMatch : getFixture().getMatches()) {
            addTeam(new BasketballTeam(basketballMatch.getHomeTeamName(), this));
        }

        evaluateTeamScores();
    }
}
